<?php

function generateShortUrl($url) {
    $hash = md5($url);
    $shortUrl = substr($hash, 0, 6);
    return $shortUrl;
}


$originalUrl = $_POST['original_url'];

$shortUrl = generateShortUrl($originalUrl);

function saveUrlPair($shortUrl, $originalUrl) {
    $filename = 'urls.txt';
    $file = fopen($filename, 'a');
    fwrite($file, $shortUrl . ' - ' . $originalUrl . PHP_EOL);
    fclose($file);
}

function getOriginalUrl($shortUrl) {

    $filename = 'urls.txt';


    if (file_exists($filename)) {
        $file = fopen($filename, 'r');
        while (!feof($file)) {
            $line = fgets($file);
            list($storedShortUrl, $originalUrl) = explode(' - ', $line);

            if ($storedShortUrl === $shortUrl) {
                fclose($file);
                return trim($originalUrl);
            }
        }

        fclose($file); 
    }

    return null;
}

saveUrlPair($shortUrl, $originalUrl);

echo 'http://yourdomain/' . $shortUrl 

?>