$(document).ready(function() {
    $('#urlForm').submit(function(event) {
        event.preventDefault();
        var originalUrl = $('#originalUrl').val();
        shortenUrl(originalUrl);
    });

    $('#copyButton').click(function() {
        var shortUrl = $('#shortUrl a').attr('href');
        copyToClipboard(shortUrl);
    });

    $('#shortUrl').on('click', 'a', function(event) {
        event.preventDefault();
        var originalUrl = $('#originalUrl').val();
        window.location.href = originalUrl;
    });
});

function shortenUrl(originalUrl) {
    $.ajax({
        url: 'short.php',
        type: 'POST',
        data: {original_url: originalUrl},
        success: function(data) {
            displayShortUrl(data);
        },
        error: function() {
            alert('Ошибка при сокращении URL');
        }
    });
}

function displayShortUrl(shortUrl) {
    $('#shortUrl').html('<a href="' + shortUrl + '" target="_blank">' + shortUrl + '</a>');
}

function copyToClipboard(text) {
    var $temp = $('<input>');
    $('body').append($temp);
    $temp.val(text).select();
    document.execCommand('copy');
    $temp.remove();
    alert('Короткий URL скопирован!');
}