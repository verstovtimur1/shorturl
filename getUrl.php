<?php
// Функция для получения оригинального URL из файла
function getOriginalUrl($shortUrl) {
   
    $filename = 'urls.txt';
    if (file_exists($filename)) {
        $file = fopen($filename, 'r');

        while (!feof($file)) {
            $line = fgets($file);
            list($storedShortUrl, $originalUrl) = explode(' - ', $line);

            if ($storedShortUrl === $shortUrl) {
                fclose($file); 
                return trim($originalUrl); // Удаляем пробелы и переносы строк
            }
        }

        fclose($file); 
    }

    return null;
}

// Проверяем, был ли передан короткий URL в запросе
if (isset($_GET['short_url'])) {
    $shortUrl = $_GET['short_url'];

    $originalUrl = getOriginalUrl($shortUrl);

    if ($originalUrl) {
        header("Location: $originalUrl");
        exit();
    }
}

http_response_code(404);
echo "Страница не найдена";
?>